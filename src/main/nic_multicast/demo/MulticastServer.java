package main.nic_multicast.demo;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class MulticastServer {

	public static void main(String[] args) throws InterruptedException, IOException {
		DatagramSocket server = new DatagramSocket();
		System.out.println("Server started...");
		int i = 0;
		while(true) {
			String mess = " efsfasdgfg" + i++;
			DatagramPacket packet = 
					new DatagramPacket(mess.getBytes(), mess.getBytes().length, 
							InetAddress.getByName("224.2.2.3"), 1107);
			server.send(packet);
			Thread.sleep(5000);
		}

	}

}
