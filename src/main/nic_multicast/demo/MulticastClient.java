package main.nic_multicast.demo;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class MulticastClient {

	public static void main(String[] args) throws UnknownHostException, IOException {
		MulticastSocket client = new MulticastSocket(1107);
		client.joinGroup(InetAddress.getByName("224.2.2.3"));
		
		while(true) {
			byte[] data = new byte[1024];
			DatagramPacket packet = 
					new DatagramPacket(data, data.length);
			client.receive(packet);
			
			System.out.println(new String(packet.getData()).trim());
		}
		

	}

}
