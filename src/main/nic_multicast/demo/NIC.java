package main.nic_multicast.demo;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class NIC {

	public static void main(String[] args) throws SocketException {
		Enumeration<NetworkInterface> nic = 
				NetworkInterface.getNetworkInterfaces();
		while (nic.hasMoreElements()) {
			NetworkInterface networkInterface = (NetworkInterface) nic.nextElement();
			if (networkInterface.isUp()) {
				System.out.println(networkInterface);
			}
		}
	}

}
