/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.sevenzeroone;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import static java.lang.Math.abs;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nhuva
 */
public class Client {
    public static void main(String[] args) {
        byte[] b;
        String s;
        InputStream inp;
        OutputStream out;
        try {
            Socket client=new Socket("localhost",1107);
            System.out.println("Connected...");
            inp=client.getInputStream();
            b= new byte[inp.available()];
            inp.read(b);
            s=new String(b);
            s.trim();
            
            String[] ss=s.split(",");
            int[] arr=new int[ss.length];
            for(int i=0;i<arr.length;i++){
                arr[i]=Integer.parseInt(ss[i]);
            }
            
            int min=10000,temp1 = 0,temp2=0;
            for(int i=0;i<arr.length;i++){
                for(int j=i+1;j<arr.length;j++){
                    int temp=abs(arr[i]-arr[j]);
                    if(temp<min){
                        min=temp;
                        temp1=arr[i];
                        temp2=arr[j];
                    }
                }
            }
            out=client.getOutputStream();
            out.write(min);
            out.write(temp1);
            out.write(temp2);
            out.close();
            inp.close();
            client.close();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
