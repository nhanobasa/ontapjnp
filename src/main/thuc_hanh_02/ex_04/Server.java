package main.thuc_hanh_02.ex_04;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	private ServerSocket server;
	private Socket socket;
	private InputStream in;
	private OutputStream out;

	public Server() {
		try {
			server = new ServerSocket(1110);
			System.out.println("Server is Running...!");
			String s = "1,3,9,19,33,20";
			while(true) {
				socket = server.accept();
				System.out.println("new connection...");
				
				// Khoi tao luong
				in = socket.getInputStream();
				out = socket.getOutputStream();
				
				//  Nhan ma Sinh vien.
				byte[] b = new byte[in.available()];
				in.read(b);				
				String strIn = new String(b);
				System.out.println("Sinh vien tham gia : " + strIn);
				
				// gui chuoi de bai
				out.write(s.getBytes());
				
				// gui object
				out.write(new Student(0, "b17dcat136", 3f, "B").toString().getBytes());
				
				in.close();
				out.close();
				socket.close();
				
			}
			
		} catch (IOException e) {			
			e.printStackTrace();
		}
		
		
	}

	public static void main(String[] args) {
		new Server();
	}

}
