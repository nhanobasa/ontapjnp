package main.thuc_hanh_02.ex_04;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

	private Socket socket;
	private InputStream in;
	private OutputStream out;
	
	public Client() {
		try {
			socket = new Socket("localhost", 1110);
			
			out = socket.getOutputStream();
			in = socket.getInputStream();		
			
			// Gui ma sinh vien.
			out.write("b17dcat136".getBytes());
			
			// Nhan chuoi de bai.
			String strIn = new String(in.readAllBytes());
			System.out.println("S = " + strIn);
			
			//Nhan object 
			strIn = new String(in.readAllBytes());
			System.out.println(strIn);
			out.close();
			socket.close();
		} catch (UnknownHostException e) {			
			e.printStackTrace();
		} catch (IOException e) {		
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Client();
	}

}
