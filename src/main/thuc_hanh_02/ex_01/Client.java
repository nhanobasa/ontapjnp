package main.thuc_hanh_02.ex_01;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", 1107);
        DataInputStream in = new DataInputStream(socket.getInputStream());
        DataOutputStream out = new DataOutputStream(socket.getOutputStream());

        // Gui ma sinh vien
        out.writeUTF("B17DCAT136");

        // Nhan 2 so nguyen a va b
        int a = in.readInt();
        int b = in.readInt();
        System.out.println("a = " + a + ", b = " + b);

        // Thuc hien gui sum , mul, ucln, bcnn
        out.writeInt(a + b);
        out.writeInt(a * b);
        out.writeInt(gcd(a, b));
        out.writeInt(a * b / gcd(a, b));

        // Nhan ket qua
        System.out.println(in.readUTF());
        socket.close();
    }

    private static int gcd(int a, int b) {
        if (a == 0)
            return b;
        return gcd(b % a, a);
    }
}
