package main.thuc_hanh_02.ex_01;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;

public class Server {
    public static void main(String[] args) throws IOException {
        @SuppressWarnings("resource")
        ServerSocket server = new ServerSocket(1107);
        System.out.println("Server is running...");
        var pool = Executors.newFixedThreadPool(100);
        while (true) {
            pool.execute(new Listening(server.accept()));
        }
    }

    private static class Listening extends Thread {
        private final Socket socket;
        private DataInputStream in;
        private DataOutputStream out;
        private final int a;
        private final int b;
        private int sum;
        private int mul;
        private int ucln;
        private int bcnn;

        private Listening(Socket socket) {
            this.socket = socket;
            a = 8;
            b = 6;
        }

        @Override
        public void run() {
            System.out.println("New connection!");
            try {
                in = new DataInputStream(socket.getInputStream());
                out = new DataOutputStream(socket.getOutputStream());

                String strMaSV = in.readUTF();
                System.out.println("Ma sv : " + strMaSV);

                // gui de bai
                out.writeInt(a);
                out.writeInt(b);

                // nhan ket qua
                sum = in.readInt();
                mul = in.readInt();
                ucln = in.readInt();
                bcnn = in.readInt();

                //check ket qua
                String strMessage = "Sinh vien + \" + strMaSV + \" da lam sai!";
                if (checkResult(sum, mul, ucln, bcnn)) {
                    strMessage = "Sinh vien + " + strMaSV + " da lam dung!";
                }
                System.out.println(strMessage);
                out.writeUTF(strMessage);
                socket.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private boolean checkResult(int sum, int mul, int ucln, int bcnn) {
            return sum == a + b && mul == a * b && ucln == gcd(a, b) && bcnn == (a * b) / gcd(a, b);
        }

        private int gcd(int a, int b) {
            if (a == 0)
                return b;
            return gcd(b % a, a);
        }
    }
}
