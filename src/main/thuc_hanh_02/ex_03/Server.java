package main.thuc_hanh_02.ex_03;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	Socket socket = null;
	ObjectInputStream ois = null;
	ObjectOutputStream oos = null;

	public Server() {
		try {
			@SuppressWarnings("resource")
			ServerSocket server = new ServerSocket(1109);
			System.out.println("Server is Running..!");
			while (true) {
				socket = server.accept();
				System.out.println("New Connection...!");
				
				// Luu y: khoi tao oos truoc ois
				oos = new ObjectOutputStream(socket.getOutputStream());
                ois = new ObjectInputStream(socket.getInputStream());
				
				System.out.println("Trao doi du lieu:");
				
				// Nhan ma sinh vien.
				String str = ois.readUTF();
                System.out.println("Ma Sinh vien: " + str);
                
                // Gui doi tuong student.
                Student student = new Student(913, "B16DCCN284", 2.7f, "");
                oos.writeObject(student);
                oos.flush();
                
                // Nhan ket qua client.
                student = (Student) ois.readObject();
                System.out.println(student);
				
				ois.close();
				oos.close();
				socket.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		new Server();
	}
}
