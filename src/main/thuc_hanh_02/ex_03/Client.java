package main.thuc_hanh_02.ex_03;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {
    private static ObjectInputStream ois;
    private static ObjectOutputStream oos;
    private static Socket client;
    private static Student student;

    public static void main(String[] args) {

        try {
            client = new Socket("localhost", 1109);
            
            // Luu y: Khoi tao oos truoc ois.
            ois = new ObjectInputStream(client.getInputStream());
            oos = new ObjectOutputStream(client.getOutputStream());
			
			System.out.println("Bat dau trao doi du lieu!");
			
            // Gui ma sinh vien.
            oos.writeUTF("B17dcat136");
            oos.flush();

            // Nhan object student.
            student = (Student) ois.readObject();
            System.out.println(student);

            // Xu ly.
            student.setGpaLetter(setGPALetter(student.getGpa()));

            // Gui ket qua.
            oos.writeObject(student);
            oos.flush();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException e) {
            System.err.println("Khong doc duoc doi tuong!");
        } finally {
            try {
                ois.close();
                oos.close();
                client.close();
            } catch (IOException e) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
            }
        }


    }

    private static String setGPALetter(float gpa) {
        if (3.7 <= gpa && gpa <= 4) {
            return "A";
        }
        if (3.0 <= gpa && gpa <= 3.7) {
            return "B";
        }
        if (2.0 <= gpa && gpa <= 3.0) {
            return "C";
        }
        if (1.0 <= gpa && gpa <= 2.0) {
            return "D";
        }
        return "F";
    }
}
