package main.thuc_hanh_02.ex_02;

import java.io.*;
import java.net.Socket;


public class Client {
    private static StringBuilder str1, str2;

    public static void main(String[] args) throws IOException {
        @SuppressWarnings("resource")
        Socket client = new Socket("localhost", 1108);
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(client.getInputStream()));
        BufferedWriter bw =
                new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));

        // Gui ma sinh vien.
        bw.write("B17dcat136");
        bw.newLine(); // phai co lenh nay no moi hieu la da sang dong moi :)
        bw.flush();

        // Nhan chuoi de bai.
        String strDb = reader.readLine();
        System.out.println(strDb);

        // Xu li chuoi.
        str1 = new StringBuilder();
        str2 = new StringBuilder();
        xuly(strDb);
        System.out.println("Str1 : " + str1.toString() + ", Str2 : " + str2.toString());
        bw.write(str1.toString());
        bw.newLine(); // phai co lenh nay no moi hieu la da sang dong moi :)
        bw.write(str2.toString());
        bw.newLine(); // phai co lenh nay no moi hieu la da sang dong moi :)
        bw.flush();

    }

    private static void xuly(String strDb) {

        for (int i = 0; i < strDb.length(); i++) {
            String strTemp = "";
            strTemp += strDb.charAt(i);
            if (strTemp.matches("[a-zA-z0-9]")) {
                str1.append(strTemp);
            } else
                str2.append(strTemp);
        }

    }
}
