package main.thuc_hanh_02.ex_02;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws IOException {
        @SuppressWarnings("resource")
        ServerSocket server = new ServerSocket(1108);
        System.out.println("Server is running...");
        while (true) {
            Socket socket = server.accept();
            System.out.println("New Connection...!");
            BufferedReader br =
                    new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter bw =
                    new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            // Nhan ma sinh vien.
            String strMaSv = br.readLine();
            System.out.println("Sinh vien " + strMaSv + " vua tham gia.");

            // Gui chuoi de bai.
            String strDb = "sdsajS7@%ahs5463A*&$#";
            bw.write(strDb);
            bw.newLine();
            bw.flush();

            String strKq = br.readLine();
            System.out.println("str1 : " + strKq);
            strKq = br.readLine();
            System.out.println("str2 : " + strKq);
            socket.close();
        }

    }
}
