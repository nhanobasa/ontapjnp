/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.nineonethree;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SClient {

    private ObjectOutputStream oos;
    private ObjectInputStream ois;
    private Socket socket;
    Student student;

    public SClient() {
        try {
            socket = new Socket("localhost", 1109);
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());
            oos.writeUTF("B16DCCN284,913");
            oos.flush();

            student = (Student) ois.readObject();
            System.out.println(student);
            setGpa();

            oos.writeObject(student);
            oos.flush();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(SClient.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                socket.close();
                oos.close();
                ois.close();
            } catch (IOException ex) {
                Logger.getLogger(SClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void setGpa() {
        if (3.7 <= student.getGpa() && student.getGpa() <= 4) {
            student.setGpaLetter("A");
        }
        if (3.0 <= student.getGpa() && student.getGpa() <= 3.7) {
            student.setGpaLetter("B");
        }
        if (2.0 <= student.getGpa() && student.getGpa() <= 3.0) {
            student.setGpaLetter("C");
        }
        if (1.0 <= student.getGpa() && student.getGpa() <= 2.0) {
            student.setGpaLetter("D");
        }
        if (0 <= student.getGpa() && student.getGpa() <= 1.0) {
            student.setGpaLetter("F");
        }
    }

    public static void main(String[] args) {
        new SClient();
    }
}
