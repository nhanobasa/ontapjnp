/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.nineonethree;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SServer {

    private ObjectOutputStream oos;
    private ObjectInputStream ois;
    private ServerSocket serverSocket;
    //Student student = new Student("Do Thanh Quang", 3.8, "");

    public SServer() {
        Student student = new Student(913, "B16DCCN284", 4.0f, "");
        try {
            serverSocket = new ServerSocket(1109);
            System.out.println("...");
            while (true) {
                Socket socket = serverSocket.accept();
                oos = new ObjectOutputStream(socket.getOutputStream());
                ois = new ObjectInputStream(socket.getInputStream());

                String str = ois.readUTF();
                System.out.println(str);

                oos.writeObject(student);
                oos.flush();

                student = (Student) ois.readObject();
                System.out.println(student);
            }
        } catch (IOException ex) {
            Logger.getLogger(SServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        new SServer();
    }
}

class Student implements Serializable {

    private static final long serialVersionUID = 20151107;

    private int id;
    private String code;
    private float gpa;
    private String gpaLetter;

    public Student(int id, String codeString, float gpa, String gpaLetter) {
        this.id = id;
        this.code = codeString;
        this.gpa = gpa;
        this.gpaLetter = gpaLetter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String codeString) {
        this.code = codeString;
    }

    public float getGpa() {
        return gpa;
    }

    public void setGpa(float gpa) {
        this.gpa = gpa;
    }

    public String getGpaLetter() {
        return gpaLetter;
    }

    public void setGpaLetter(String gpaLetter) {
        this.gpaLetter = gpaLetter;
    }

    @Override
    public String toString() {
        return "Student : " + " Id: " + id + ", Code: " + code + ", Gpa: " + gpa + ", GpaLetter: " + gpaLetter;
    }

}
