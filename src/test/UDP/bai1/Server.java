/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.UDP.bai1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nhani
 */
public class Server {

    DatagramSocket server;
    DatagramPacket receivePacket, sendPacket;
    int port;
    int bufferSize;
    byte[] receiveData, sendData;

    public Server() {
        try {
            port = 1107;
            bufferSize = 1024;
            server = new DatagramSocket(port);
            System.out.println("Server is opening...");
        } catch (SocketException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void listening() {
        System.out.println("Listening...");
        while (true) {
            // Nhan ma_sinh_vien; ma_cau_hoi
            String strTemp = receive();
            System.out.println("Siinh vien dang thuc hien: " + strTemp);

            // Gui chuoi can xu ly voi hai so a, b ngay nhien.
            int a = (int) (Math.random() * 100);
            int b = (int) (Math.random() * 100);
            strTemp = "requestId;" + a + "," + b;
            System.out.println(strTemp);
            send(strTemp);

            // Nhan ket qua xu li tu client.
            strTemp = receive();
            System.out.println("Ket qua thuc hien: " + strTemp);

        }
    }

    private String receive() {
        // Khoi tao mang byte
        receiveData = new byte[bufferSize];
        receivePacket = new DatagramPacket(receiveData, receiveData.length);
        try {
            // Nhan du lieu
            server.receive(receivePacket);
            return new String(receivePacket.getData()).trim();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void send(String strData) {
        // Chuyen String vao mang byte
        sendData = strData.getBytes();
        sendPacket = new DatagramPacket(sendData, sendData.length, receivePacket.getSocketAddress());        
        try {
            server.send(sendPacket);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void main(String[] args) {
        Server server = new Server();
        server.listening();
    }
}
