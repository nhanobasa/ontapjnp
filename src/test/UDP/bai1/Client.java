/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.UDP.bai1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nhani
 */
public class Client {

    private DatagramSocket client;
    private DatagramPacket receivePacket, sendPacket;
    private int port;
    private int bufferSize;
    private byte[] sendData, receiveData;

    public Client() {
        port = 1107;
        bufferSize = 1024;

        try {
            client = new DatagramSocket();
            System.out.println("Client is starting...");
        } catch (SocketException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void listening() {
        System.out.println("Listening...");

        // Gui ma_sinh_vien; ma_cau_hoi
        String strTemp = "B17DCAT136;01";
        send(strTemp);

        // Nhan chuoi can xu li
        strTemp = receive();
        System.out.println("Chuoi can xu ly: " + strTemp);

        //xu li
        String[] arrs = strTemp.split(";");
        String[] val = arrs[1].split(",");
        int a = Integer.parseInt(val[0]);
        int b = Integer.parseInt(val[1]);

        int ucln = ucln(a, b);
        int bcnn = (a * b) / ucln;
        int sum = a + b;
        int mul = a * b;
        String result = arrs[0] + ";" + ucln + "," + bcnn + "," + sum + "," + mul;            
        send(result);

        
    }

    private void send(String strData) {
        sendData = strData.getBytes();
        try {
            sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName("localhost"), port);
            client.send(sendPacket);
        } catch (UnknownHostException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private String receive() {
        receiveData = new byte[bufferSize];
        receivePacket = new DatagramPacket(receiveData, receiveData.length, sendPacket.getSocketAddress());

        try {
            // Nhan du lieu.
            client.receive(receivePacket);
            return new String(receivePacket.getData()).trim();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    private static int ucln(int a, int b) {
        if (a == 0) {
            return b;
        }
        return ucln(b % a, a);
    }

    public static void main(String[] args) {
        Client c = new Client();
        c.listening();
    }
}
