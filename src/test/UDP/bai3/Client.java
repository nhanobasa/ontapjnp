/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.UDP.bai3;

import UDP.Student;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nhani
 */
public class Client {
    DatagramSocket client;
    DatagramPacket receivePacket, sendPacket;
    byte[] receiveData, sendData;
    int bufferSize;
    int port;
    
    public Client() {
        port = 1109;
        bufferSize = 1024;
        
        try {
            client = new DatagramSocket();
            System.out.println("Client Stated...");
        } catch (SocketException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    public void listening(){
        
        
    }
    
    private void send(Student student){        
        try {
            sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName("localhost"), port);
        } catch (UnknownHostException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    private <T> T BytesToObject(byte[] bytes) {
        T t = null;
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        try {
            ObjectInputStream ois = new ObjectInputStream(bais);
            t = (T) ois.readObject();
        } catch (Exception ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return t;
    }
    
    private <T> byte[] object2Bytes(T t){
        byte[] result = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ObjectOutputStream ois = new ObjectOutputStream(baos);
            ois.flush();
            ois.writeObject(t);
            ois.flush();
            result = baos.toByteArray();
        } catch (Exception e) {
        }
        return result;
    }
        
    public static void main(String[] args) {
        Client c = new Client();
        c.listening();
    }
}
